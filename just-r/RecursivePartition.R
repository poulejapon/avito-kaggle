source("just-r/prepareSandboxFeaturesWithClick.R");

rpart_model <- train(IsClick ~ ., data = training, method = "rpart", trControl = logLossTC, metric = "LogLoss", maximize = F);

print(rpart_model);

rpart_prediction <- predict(rpart_model, testing, type = "prob");

print(LogLoss(as.numeric(testing$IsClick == 'T'), rpart_prediction$T));
