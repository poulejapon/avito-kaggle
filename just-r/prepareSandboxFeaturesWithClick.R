source("just-r/util.R")
source("just-r/LogLossSummaryFunction.R")

library(caret);

data = ReadTsv();
data <- subset(data, select = -c(ID, AdAdID));
data$IsClick[data$IsClick == 1] <- 'T'
data$IsClick[data$IsClick == 0] <- 'F'
data$IsClick <- as.factor(data$IsClick);

UserIsUserLoggedOn <- paste0('UserLoggedOn', data$UserIsUserLoggedOn == 1.0);
UserIsUserLoggedOn <- as.factor(UserIsUserLoggedOn);

UserSearchAdSameCategory <- paste0('SameCategory', data$UserSearchCategoryID == data$AdCategoryID);
UserSearchAdSameCategory <- as.factor(UserSearchAdSameCategory);

AdCategoryID <- as.factor(data$AdCategoryID);
UserSearchMonth <- as.factor(data$UserSearchMonth);
UserSearchDay <- as.factor(data$UserSearchDay);
UserSearchHour <- as.factor(data$UserSearchHour);
UserSearchCategoryID <- as.factor(data$UserSearchCategoryID);

UserLastPhoneMonth <- as.factor(data$UserLastPhoneMonth);
UserLastPhoneDay <- as.factor(data$UserLastPhoneDay);
UserLastPhoneHour <- as.factor(data$UserLastPhoneHour);

#UserAgentID <- as.factor(data$UserAgentID);
#UserAgentOSID <- as.factor(data$UserAgentOSID);
#UserDeviceID <- as.factor(data$UserDeviceID);
#UserAgentFamilyID <- as.factor(data$UserAgentFamilyID);

features <- subset(data, select = c(Position, HistCTR, UserNbClicks, UserNbDisplays, AdNbClicks, AdNbDisplays, AdFreshness, UserPhoneCount, AdPriceRankByCat));
scaler <- preProcess(features, method = c("center", "scale"));
data_norm <- data.frame(predict(scaler, features), UserIsUserLoggedOn = UserIsUserLoggedOn, UserSearchDay = UserSearchDay, UserSearchHour = UserSearchHour, UserSearchAdSameCategory = UserSearchAdSameCategory, IsClick = data$IsClick);

# , AdCategoryID = AdCategoryID, UserSearchMonth = UserSearchMonth, UserSearchCategoryID = UserSearchCategoryID, UserLastPhoneMonth = UserLastPhoneMonth, UserLastPhoneDay = UserLastPhoneDay, UserLastPhoneHour = UserLastPhoneHour);

set.seed(12345);

# TODO: currently downsampling to put everything on memory
# inTraining <- createDataPartition(data_norm$IsClick, p = 0.1, list = FALSE);
# data_norm <- data_norm[inTraining,];

inTraining = createDataPartition(data_norm$IsClick, p = 0.7, list = FALSE);
training = data_norm[inTraining,];
testing = data_norm[-inTraining,];
