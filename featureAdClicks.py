from dataset import Dataset, RawDataset

ad_clicks = Dataset("adsClickCounts")

Dataset("testSearchStreamLight")\
	.retain(["ID", "AdID"])\
	.hash_join("AdID", ad_clicks)\
	.prefix("Ad")\
        .rename("AdID", "ID")\
	.write("featureAdClicks")
