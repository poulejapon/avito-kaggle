from dataset import Dataset, RawDataset, GeneratorStream
from collections import defaultdict
from utils import ClickCount

user_clicks = defaultdict(ClickCount)
ads_clicks = defaultdict(ClickCount)

for (is_click, ad_id, user_id) in Dataset("testedUsersClicks").retain(["IsClick", "AdID", "UserID"]):
	user_clicks[int(user_id)].add_display(int(is_click))
	ads_clicks[int(ad_id)].add_display(int(is_click))

def flatten(click_count):
	def aux():
		for user_id, clicks in user_clicks.iteritems():
			yield user_id, clicks.clicks, clicks.displays
	return aux

GeneratorStream(flatten(user_clicks), ["UserID", "NbClicks", "NbDisplays"])\
	.write("testedUsersClickCounts")

GeneratorStream(flatten(ads_clicks), ["AdID", "NbClicks", "NbDisplays"])\
	.write("adsClickCounts")
