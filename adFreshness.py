from dataset import RawDataset

ad_ids = RawDataset("AdsInfo").retain(["AdID"])

max_ad_id = 0
for row in ad_ids:
    ad_id = int(row[0])
    if max_ad_id < ad_id: max_ad_id = ad_id

ad_ids.map(lambda row: (row[0], int(row[0]) - max_ad_id), (["AdID", "AdFreshness"]))\
      .write("adFreshness")
