#!/usr/bin/env python

"""
Creates a list of search ids that are
retained to create the test session.
"""

from dataset import RawDataset, SimpleStream, Dataset
from collections import Counter

user_last_session = {}

search_info = RawDataset("SearchInfo")\
	.retain(["SearchID", "UserID"])\

search_stream = RawDataset("trainSearchStream")

joined = search_stream\
	.retain(["SearchID", "ObjectType"])\
	.filter(lambda row:row[1] == "3")\
	.left_join("SearchID", search_info)

for (search_id, _, user_id) in joined:
	user_last_session[int(user_id)] = int(search_id)

test_sessions = sorted(user_last_session.values())
SimpleStream(test_sessions, "SearchID")\
	.write("testSearchIds")
