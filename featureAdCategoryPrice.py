from dataset import Dataset, RawDataset

ads_info = RawDataset("AdsInfo")\
           .retain(["AdID", "CategoryID", "Price"])

Dataset("testSearchStreamLight")\
	.retain(["ID", "AdID"])\
	.hash_join("AdID", ads_info)\
        .drop("AdID")\
        .rename("CategoryID", "AdCategoryID")\
	.write("featureAdCategoryPrice")
