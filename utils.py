class ClickCount(object):
	__slots__ = ('clicks', 'displays')
	def __init__(self,):
		self.clicks = 0
		self.displays = 0

	def add_display(self, is_click):
		self.displays += 1
		self.clicks += is_click
