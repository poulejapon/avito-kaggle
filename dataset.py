"""
Helper module.

Helps reading datasets and enforce magically the separation 
of sandbox, raw, and submission
"""

import csv
from os import path as osp
from collections import Counter
import subprocess
import sys
import argparse
from contextlib import contextmanager	
import pandas as pd

parser = argparse.ArgumentParser(description='Generic script')
parser.add_argument('--inputs', metavar='<inputfile>', type=str, nargs='+', help="Input files")
parser.add_argument('--output', metavar='<outputfile>', type=str, help="Output file")
parsed = parser.parse_args()

def extract_dataset_name(filepath):
	return osp.splitext(osp.basename(filepath))[0]

TARGET = extract_dataset_name(parsed.output)
INPUTS = [
	extract_dataset_name(input_dataset)
	for input_dataset in parsed.inputs
	if input_dataset.endswith(".tsv")
]
MODE = osp.basename(osp.dirname(parsed.output))
print "TARGET:", TARGET
print "INPUTS:", INPUTS
print "MODE:", MODE

"""
MODE_PATH can be either 
 	- ./datasets/sandbox
 	for training
 	- ./datasets/submission
 	for actually producing a dataset to submit to kaggle.

To avoid accidents io.py only allows to write and read from one target path.
The mode is detected from the targets of the script given in arguments.
"""

MODE_PATH = osp.join("datasets", MODE)
CSV_CONFIG = {"delimiter": "\t"}


def check_columns(columns):
	for col in columns:
		if not isinstance(col, str):
			raise ValueError("Column %s is not a string" % str(col))
	column_counts = Counter(columns)
	duplicated = [col for (col, count) in column_counts.items() if count > 1]
	if len(duplicated) > 0:
		raise ValueError("Columns %s are duplicated" % duplicated)



class Stream(object):

	def __init__(self, columns):
		check_columns(columns)
		self.columns = columns
		self.column_map = dict([(col, cid) for (cid, col) in enumerate(self.columns)])

	def __iter__(self,):
		raise NotImplementedError()

	@property
	def nb_cols(self,):
		return len(self.columns)

	def col(self, colname):
		if colname not in self.column_map:
			raise Exception("Column \"%s\" does not exist in %s" %(colname, self))
		return self.column_map[colname]

	def head(self, limit):
		return Head(self, limit) 

	def prefix(self, s):
		return Rename(self, columns=[s + col for col in self.columns])

	def rename(self, before, after):
		return Rename(self, columns=[ after if before == col else col for col in self.columns])

	def __repr__(self,):
		return "Stream([%s])" % ",".join(self.columns)

	def retain(self, columns):
		return SelectedColumns(self, columns)

	def drop(self, *columns):
		return SelectedColumns(self, [col for col in self.columns if col not in columns])

	def map(self, mapper, columns):
		return MappedColumns(self, columns, mapper)

	def remove(self, cols_to_remove):
		columns = tuple(col for col in self.columns if col not in set(cols_to_remove))
		return self.retain(columns)

	def left_join(self, key, *args):
		return ZipMerge([(self, key)] + [(branch, key) for branch in args])

	def hash_join(self, key, right, default_value=0):
		return HashJoin(self, right, key, default_value=0)

	def filter(self, predicate):
		return FilteredStream(self, predicate)

	def values_set(self, key, cast=int):
		vals = set()
		for (val,) in self.retain((key,)):
			vals.add(cast(val))
		vals = sorted(vals)
		return SimpleStream(vals, key)

	def write(self, name, log_every=1000000):
		filepath = osp.join(MODE_PATH, name + ".tsv")
		with open(filepath, 'wb') as output:
			writer = csv.writer(output, **CSV_CONFIG)
			writer.writerow(self.columns)
			nb_rows = 0
			nb_cols = self.nb_cols
			for row in self:
				if len(row) != nb_cols:
					raise ValueError("Found %d values for %d cols" % (len(row), nb_cols))
				writer.writerow(row)
				nb_rows += 1
				if nb_rows % log_every == 0:
					print "written %i rows in %s" % (nb_rows, filepath)


def write_dataframes(datasetname, dfs):
	dataset_path = resolve_dataset(datasetname)
	dfs = iter(dfs)
	first = True
	with open(dataset_path, 'wb') as f:
		for df in dfs:
			df.to_csv(f, index=False, sep='\t', header=first)
			first = False


class SimpleStream(Stream):
	def __init__(self, vals, column):
		Stream.__init__(self, [column])
		self.vals = vals

	def __iter__(self,):
		for val in self.vals:
			yield (val,)

class UnderlyingDataSetStream(Stream):
        def __init__(self, underlying, columns):
                Stream.__init__(self, columns)
                self.underlying = underlying

        def get_dataframe(self):
                return self.underlying.get_dataframe()

class Rename(UnderlyingDataSetStream):
	def __iter__(self,):
		return iter(self.underlying)

class GeneratorStream(Stream):
	def __init__(self, gen, columns):
		Stream.__init__(self, columns)
		self.gen = gen

	def __iter__(self,):
		return iter(self.gen())

class SelectedColumns(UnderlyingDataSetStream):

	def __init__(self, underlying, columns):
		UnderlyingDataSetStream.__init__(self, underlying, columns)
		self.column_ids = [underlying.col(col) for col in columns]

	def __iter__(self,):
		for row in self.underlying:
			yield tuple(row[col_id] for col_id in self.column_ids)

class FilteredStream(UnderlyingDataSetStream):

	def __init__(self, underlying, predicate):
		UnderlyingDataSetStream.__init__(self, underlying, underlying.columns)
		self.predicate = predicate

	def __iter__(self,):
		predicate = self.predicate
		for row in self.underlying:
			if predicate(row):
				yield row

class MappedColumns(UnderlyingDataSetStream):

	def __init__(self, underlying, columns, mapper):
		UnderlyingDataSetStream.__init__(self, underlying, columns)
		self.mapper = mapper

	def __iter__(self,):
		for row in self.underlying:
			yield self.mapper(row)

def is_sorted(column_ids):
	for (prev, next) in zip(column_ids, column_ids[1:]):
		if next < prev:
			return False
	return True


class Head(Stream):

	def __init__(self, underlying, limit):
		self.underlying = underlying
		self.limit = limit
		Stream.__init__(self, self.underlying.columns)

	def __iter__(self,):
		nb_rows = 0
		for row in self.underlying:
			yield row
			nb_rows += 1
			if nb_rows == self.limit:
				break


class UnixPipe(object):

	def __init__(self, commands):
		self.commands = commands

	@staticmethod
	def cat(filepath):
		if not osp.exists(filepath):
			raise IOError("File not found %s" % filepath)	
		return UnixPipe(["cat " + filepath])

	def pipe(self, command):
		return UnixPipe(self.commands + [command])

	def command(self,):
		return " | ".join(self.commands)

	def cut(self, column_ids):
		assert is_sorted(column_ids)
		column_ids = [str(col + 1) for col in column_ids]
		return self.pipe("cut -f " + ",".join(column_ids))

	def head(self, limit):
		return self.pipe("head -n %i" % limit)

	def __repr__(self,):
		return "UnixPipe(%s)" % (self.command())

	@contextmanager
	def open(self,):
		cmd = self.command()
		print cmd
		proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=sys.stderr, shell=True)
		try:
			yield proc.stdout
		finally:
			proc.terminate()
			
class HashJoin(Stream):

	def __init__(self, left_stream, right_dataset, column, default_value=0):
		self.left_stream = left_stream
		self.right_dataset = right_dataset
		self.column = column
		columns = self.left_stream.columns + tuple(col for col in right_dataset.columns if col != column)
		self.default_value = (default_value,) * (len(right_dataset.columns) - 1)
		Stream.__init__(self, columns)

	def __iter__(self,):
		df = self.right_dataset.get_dataframe().set_index(self.column)
		cid = self.left_stream.col(self.column)
		for row in self.left_stream:
			k = int(row[cid])
			if k in df.index:
				yield row + tuple(df.loc[k])
			else:
				yield row + self.default_value

def resolve_dataset(dataset_name, mode=MODE_PATH):
	return osp.join(mode, dataset_name + ".tsv")


class Dataset(Stream):

	RESOLVE_PATH = MODE_PATH

	def __init__(self, name, columns=None, unixpipe=None):
		self.name = name
		if unixpipe is not None:
			self.unixpipe = unixpipe
		else:
			self.unixpipe = UnixPipe.cat(self.resolve())
		self.has_header = columns is None
		if self.has_header:
			columns = self.rows(skip_header=False).next()
		Stream.__init__(self, columns)

	def __open(self,):
		return self.unixpipe.open()

	def get_dataframe(self,):
		with self.__open() as csvfile:
			if self.has_header:
				return pd.read_csv(csvfile, delimiter='\t')
			else:
				return pd.read_csv(csvfile, delimiter='\t', names=self.columns)	

	def pipe(self, command):
		return Dataset(self.name, unixpipe=self.unixpipe.pipe(command))

	def __iter__(self,):
		""" returns a stream of tuples"""
		return self.rows(skip_header=self.has_header)

	def retain(self, columns):
		col_ids = [self.col(column) for column in columns]
		dataset = self
		if len(columns) < len(self.columns):
			unixpipe = self.unixpipe.cut(sorted(col_ids))
			new_columns = None 
			if not self.has_header:
				new_columns = columns
			dataset = Dataset(self.name, unixpipe=unixpipe, columns=new_columns)
		if is_sorted(col_ids):
			return dataset
		else:
			return SelectedColumns(dataset, columns)

	def resolve(self,):
		return resolve_dataset(self.name, mode=self.__class__.RESOLVE_PATH)

	def rows(self, skip_header=True, log_every=1000000):
		# For performance reason we don't decode utf-8
		# fortunately, csv encoding is commutative with utf-8 
		# decoding
		# Please decode your fields manually
		nb_rows = 0
		#with open(self.filepath, 'rb') as csvfile:
		with self.__open() as csvfile:
			rows_it = csv.reader(csvfile, **CSV_CONFIG)
			if skip_header:
				rows_it.next()
			for row in rows_it:
				nb_rows += 1
				yield tuple(row)
				if nb_rows % log_every == 0:
					print "read %i rows from %s" % (nb_rows, self)


class RawDataset(Dataset):
	""" Raw dataset are dataset as they have been
	downloaded.
	"""
	RESOLVE_PATH = "./datasets/raw/"


class Seeker(object):

	__slots__ = ('it', 'key_id', 'key_val', 'key_func', 'row', 'retained_values')

	def __init__(self, stream, key):
		self.it = iter(stream)
		self.key_id = stream.col(key)
		self.key_func = lambda row:int(row[self.key_id])
		self.key_val = None
		self.row = None
		self.next()
		self.retained_values = [
			i for i in range(stream.nb_cols)
			if i != self.key_id
		]

	def next(self,):
		self.row = self.it.next()
		self.key_val = self.key_func(self.row)

	def get(self, key_val):
		while self.key_val < key_val:
			self.next()
		if self.key_val == key_val:
			return tuple(self.row[i] for i in self.retained_values)
		else:
			return None


class ZipMerge(Stream):

	def __init__(self, joins):
		self.joins = joins
		columns = list(joins[0][0].columns)
		for (stream, key) in joins[1:]:
			for col in stream.columns:
				if col != key:
					columns.append(col)
		Stream.__init__(self, tuple(columns))

	def __iter__(self,):
		seekers = [Seeker(stream, key) for (stream, key) in self.joins]
		(left_branch, join_seekers) = (seekers[0], seekers[1:]) 
		while True:
			current_row = left_branch.next()
			current_key = left_branch.key_val
			build_row = [left_branch.row] 
			for join_reader in join_seekers:
				row = join_reader.get(current_key)
				build_row.append(row)
			yield sum(build_row, ())
