from dataset import Dataset, RawDataset

search_info_stream = RawDataset("SearchInfo")\
	.retain(["SearchID", "UserID", "SearchDate", "IsUserLoggedOn", "CategoryID"])

tested_users = set(Dataset("testedUsers").get_dataframe()["UserID"])

user_cid = search_info_stream.col("UserID")

def expand_search_date(row):
    sid, uid, sdate, loggedon, cat_id = row
    date, time = sdate.split()
    year, month, day = date.split('-')
    hour, _, _ = time.split(':')
    return (sid, uid, month, day, hour, loggedon, cat_id)

search_info_stream\
	.filter(lambda row: int(row[user_cid]) in tested_users)\
        .map(expand_search_date, ["SearchID", "UserID", "SearchMonth", "SearchDay", "SearchHour", "IsUserLoggedOn", "SearchCategoryID"])\
	.write("testedUsersSearchDateLoggedOnCategory")
