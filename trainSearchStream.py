#!/usr/bin/env python

"""
Creates the sandbox train and test streams.

TASK 
Pull the click column from testSearchStream and store
it in a separate dataset.
"""

from dataset import RawDataset, SimpleStream, Dataset
from collections import Counter

test_search_ids = set()

for (search_id,) in Dataset("testSearchIds"):
	test_search_ids.add(int(search_id))

train_search_stream = RawDataset("trainSearchStream")
search_cid = train_search_stream.col("SearchID")

predicate_train = lambda row: int(row[search_cid]) not in test_search_ids
train = train_search_stream.filter(predicate_train)
train.write("trainSearchStream")
