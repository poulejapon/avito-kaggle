from dataset import Dataset

user_phone_info = Dataset("testedUsersPhoneRequests")

Dataset("testSearchStreamLight")\
	.retain(["ID", "SearchID"])\
	.hash_join("SearchID", user_phone_info)\
	.drop("SearchID")\
	.write("featureUsersPhoneRequests")
