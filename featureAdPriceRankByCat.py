from dataset import Dataset, RawDataset
from collections import defaultdict
import math

class AdInfo:
    def __init__(self):
        self.price = 0
        self.price_rank = 0.0

class AdsByCategory:
    def __init__(self):
        self.ads = []

    def add(self, ad):
        self.ads.append(ad)

    def normalize(self):

        if len(self.ads) == 0:
            return

        ads = sorted(self.ads, lambda x, y: int(x.price - y.price))
        size = len(self.ads)
        for i, ad in enumerate(ads):
            ad.price_rank = float(i) / size

ads = defaultdict(AdInfo)
category_ads = defaultdict(AdsByCategory)

for ad_id, category_id, price in RawDataset("AdsInfo").retain(["AdID", "CategoryID", "Price"]):
    ad = ads[ad_id]
    if price:
        ad.price = float(price)
    category_ads[category_id].add(ad)

for i, category in category_ads.iteritems():
    category.normalize()

def get_price_rank(row):
    id, ad_id = row
    return (id, ads[ad_id].price_rank)

Dataset("testSearchStreamLight")\
	.retain(["ID", "AdID"])\
        .map(get_price_rank, ["ID", "AdPriceRankByCat"])\
	.write("featureAdPriceRankByCat")
