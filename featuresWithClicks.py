from dataset import Dataset, write_dataframes, INPUTS, TARGET
from itertools import izip
import pandas as pd

def iter_dataframe(datasetname):
	return pd.read_csv(Dataset(datasetname).resolve(), iterator=True, delimiter='\t', chunksize=1000)

df_it = [
	iter(iter_dataframe(feature))
	for feature in INPUTS
]

output_df_it = (
	pd.concat(df_its, axis=1)
	for df_its in izip(*df_it)
)

write_dataframes(TARGET, output_df_it)
