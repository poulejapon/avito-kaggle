
from dataset import RawDataset, SimpleStream, Dataset, GeneratorStream
from collections import Counter

test_search_ids = set(Dataset("testSearchIds").get_dataframe()["SearchID"])
train_search_stream = RawDataset("trainSearchStream")
search_cid = train_search_stream.col("SearchID")
predicate_test = lambda row: int(row[search_cid]) in test_search_ids

test_stream_without_id = train_search_stream.filter(predicate_test)

def append_id(iterable):
	def aux():
		for (i, row) in enumerate(iter(iterable)):
			yield (i,) + row
	return aux

if __name__== "__main__":
	test_stream_with_click = GeneratorStream(append_id(test_stream_without_id), ("ID",) + test_stream_without_id.columns) 
	test_stream_with_click\
		.remove(["IsClick"])\
		.write("testSearchStream")
