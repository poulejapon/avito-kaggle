from dataset import Dataset

user_search_info = Dataset("testedUsersSearchDateLoggedOnCategory")

Dataset("testSearchStreamLight")\
	.retain(["ID", "SearchID"])\
	.hash_join("SearchID", user_search_info)\
	.retain(["ID", "SearchMonth", "SearchDay", "SearchHour", "IsUserLoggedOn", "SearchCategoryID"])\
	.prefix("User")\
        .rename("UserID", "ID")\
	.write("featureUserSearchDateLoggedOnCategory")

