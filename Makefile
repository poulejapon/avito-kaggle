.SECONDARY:

.PHONY: install all clean Rsetup
Makefile:
	echo "Hello"

install: venv
	chmod -w datasets/raw/*

venv:
	virtualenv venv
	pip install -r requirements.txt
	touch venv

Rsetup: just-r/setup.R
	Rscript just-r/setup.R

sandbox: datasets/sandbox/featuresWithClicks.tsv

submission: datasets/submission/featuresWithClicks.tsv

clean:
	rm -fr datasets/sandbox/*
	rm -fr datasets/submission/*

#-----------------------
# Sandbox creation
#
datasets/sandbox/testSearchIds.tsv: testSearchIds.py datasets/raw/SearchInfo.tsv datasets/raw/trainSearchStream.tsv
	# outputs a list of search ids used for our sandbox
	python testSearchIds.py --inputs $^ --output $@

datasets/sandbox/trainSearchStream.tsv: trainSearchStream.py datasets/sandbox/testSearchIds.tsv
	python trainSearchStream.py  --inputs $^ --output $@

datasets/sandbox/testSearchStream.tsv: testSearchStream.py datasets/sandbox/testSearchIds.tsv
	python testSearchStream.py  --inputs $^ --output $@

datasets/sandbox/target.tsv: target.py datasets/sandbox/testSearchIds.tsv
	python target.py --input $^ --output $@

datasets/submission/trainSearchStream.tsv:
	ln -sf ../raw/trainSearchStream.tsv $@

datasets/submission/testSearchStream.tsv:
	ln -sf ../raw/testSearchStream.tsv $@

datasets/submission/target.tsv: datasets/submission/trainSearchStream.tsv
	cut -f 6 $^ > $@

#------------------------
# Feature creation
#
%Light.tsv: trainSearchStreamLight.py %.tsv
	python trainSearchStreamLight.py --inputs $^ --output $@

# %/trainSearchStreamExtraLight.tsv: trainSearchStreamExtraLight.py %/testedUsers.tsv %/trainSearchStreamExtraLight.tsv
# 	python trainSearchStreamExtraLight.py --inputs $^ --output $@

%/testedUsers.tsv: testedUsers.py %/testSearchStreamLight.tsv
	python testedUsers.py --inputs $^ --output $@

%/testedUsersClicks.tsv: testedUsersClicks.py %/testedUsers.tsv %/trainSearchStreamLight.tsv
	python testedUsersClicks.py --input $^ --output $@

%/testedUsersClickCounts.tsv: testedUsersClickCounts.py %/testedUsersClicks.tsv
	python testedUsersClickCounts.py --input $^ --output $@

%/adsClickCounts.tsv: testedUsersClickCounts.py %/testedUsersClicks.tsv
	python testedUsersClickCounts.py --input $^ --output $@

%/featureUserClicks.tsv: featureUserClicks.py %/testedUsersClickCounts.tsv
	python featureUserClicks.py --input $^ --output $@

%/featureAdClicks.tsv: featureAdClicks.py %/adsClickCounts.tsv
	python featureAdClicks.py --input $^ --output $@

%/featurePositionHistCTR.tsv: %/testSearchStreamLight.tsv
	cut -f 1,4,5 $^ > $@

%/adFreshness.tsv: adFreshness.py %/testSearchStreamLight.tsv
	python adFreshness.py --input $^ --output $@

%/featureAdFreshness.tsv: featureAdFreshness.py %/adFreshness.tsv
	python featureAdFreshness.py --input $^ --output $@

%/featureAdCategoryPrice.tsv: featureAdCategoryPrice.py %/testSearchStreamLight.tsv
	python featureAdCategoryPrice.py --input $^ --output $@

%/testedUsersSearchDateLoggedOnCategory.tsv: testedUsersSearchDateLoggedOnCategory.py %/testedUsers.tsv
	python testedUsersSearchDateLoggedOnCategory.py --input $^ --output $@

%/featureUserSearchDateLoggedOnCategory.tsv: featureUserSearchDateLoggedOnCategory.py %/testSearchStreamLight.tsv %/testedUsersSearchDateLoggedOnCategory.tsv
	python featureUserSearchDateLoggedOnCategory.py --input $^ --output $@

%/testedUsersPhoneRequests.tsv: testedUsersPhoneRequests.py %/testedUsers.tsv
	python testedUsersPhoneRequests.py --input $^ --output $@

%/featureUsersPhoneRequests.tsv: featureUsersPhoneRequests.py %/testSearchStreamLight.tsv %/testedUsersPhoneRequests.tsv
	python featureUsersPhoneRequests.py --input $^ --output $@

%/featureAdPriceRankByCat.tsv: featureAdPriceRankByCat.py %/testSearchStreamLight.tsv
	python featureAdPriceRankByCat.py --input $^ --output $@

%/features.tsv: features.py %/featurePositionHistCTR.tsv %/featureUserClicks.tsv %/featureAdClicks.tsv %/featureAdFreshness.tsv %/featureAdCategoryPrice.tsv %/featureUserSearchDateLoggedOnCategory.tsv %/featureUsersPhoneRequests.tsv %/featureAdPriceRankByCat.tsv
	python features.py --input $^ --output $@

%/featuresWithClicks.tsv: featuresWithClicks.py %/features.tsv %/target.tsv
	python featuresWithClicks.py --input $^ --output $@

%/LogisticRegression.RModel: just-r/LogisticRegression.R %/featuresWithClicks.tsv
	Rscript $^ $@

%/RegressionTree.RModel: just-r/RegressionTree.R %/featuresWithClicks.tsv
	Rscript $^ $@

%/NaiveBayes.RModel: just-r/NaiveBayes.R %/featuresWithClicks.tsv
	Rscript $^ $@
