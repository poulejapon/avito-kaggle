from dataset import Dataset, RawDataset

user_clicks = Dataset("testedUsersClickCounts")

search_users = RawDataset("SearchInfo")\
	.retain(["SearchID", "UserID"])

Dataset("testSearchStreamLight")\
	.retain(["ID", "SearchID"])\
	.hash_join("SearchID", search_users)\
	.hash_join("UserID", user_clicks)\
	.retain(["ID", "NbClicks", "NbDisplays"])\
	.prefix("User")\
        .rename("UserID", "ID")\
	.write("featureUserClicks")
