from dataset import Dataset, write_dataframes, INPUTS
from itertools import izip
import pandas as pd

def iter_dataframe(datasetname):
	return pd.read_csv(Dataset(datasetname).resolve(), delimiter='\t')

dfs = [
	iter_dataframe(feature)
	for feature in INPUTS
]

output_df = dfs[0]
for df in dfs[1:]:
        output_df = pd.merge(output_df, df, on = 'ID')

write_dataframes("features", [output_df])
