from dataset import Dataset, RawDataset

search_info_stream = RawDataset("SearchInfo")\
	.retain(["SearchID", "UserID"])

tested_users = set(Dataset("testedUsers").get_dataframe()["UserID"])

train_search_stream = Dataset("trainSearchStreamLight")\
	.retain(["SearchID", "AdID", "IsClick"])\
	.left_join("SearchID", search_info_stream)

user_cid = train_search_stream.col("UserID")

train_search_stream\
	.filter(lambda row: int(row[user_cid]) in tested_users)\
	.write("testedUsersClicks")

