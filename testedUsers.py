#!/usr/bin/env python

"""
Creates the list of user in the test stream.
The list can then be used to lighten other datasets. 
"""

from dataset import RawDataset, Dataset, SimpleStream

search_info = RawDataset("SearchInfo").retain(["SearchID", "UserID"])
Dataset("testSearchStreamLight")\
	.retain(["SearchID"])\
	.left_join("SearchID", search_info)\
	.values_set("UserID")\
	.write("testedUsers")
