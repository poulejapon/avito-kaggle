
from testSearchStream import test_search_ids
from dataset import RawDataset

RawDataset("trainSearchStream")\
	.retain(["SearchID", "ObjectType", "IsClick"])\
	.filter(lambda row: int(row[0]) in test_search_ids)\
	.filter(lambda row: row[1]=="3")\
	.retain(["IsClick"])\
	.write("target")
