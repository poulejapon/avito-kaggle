from dataset import Dataset, RawDataset
from collections import defaultdict

class PhoneRequest:
    def __init__(self):
        self.count = 0
        self.last_month = 0
        self.last_day = 0
        self.last_hour = 0
    def request(self, date):
        self.count += 1
        date, time = date.split()
        _, self.last_month, self.last_day = date.split('-')
        self.last_hour, _, _ = time.split(':')

user_phone_reqs = defaultdict(PhoneRequest)

search_info_stream = RawDataset("SearchInfo")\
	.retain(["SearchID", "UserID"])

for (user_id, request_date) in RawDataset("PhoneRequestsStream").retain(["UserID", "PhoneRequestDate"]):
    user_phone_reqs[int(user_id)].request(request_date)

tested_users = set(Dataset("testedUsers").get_dataframe()["UserID"])

user_cid = search_info_stream.col("UserID")

def expand_phone_info(row):
    sid, uid = row
    req = user_phone_reqs[int(uid)]
    return (sid, uid, req.last_month, req.last_day, req.last_hour, req.count)

search_info_stream\
	.filter(lambda row: int(row[user_cid]) in tested_users)\
        .map(expand_phone_info, ["SearchID", "UserID", "UserLastPhoneMonth", "UserLastPhoneDay", "UserLastPhoneHour", "UserPhoneCount"])\
	.write("testedUsersPhoneRequests")
