from dataset import Dataset, TARGET, INPUTS

"""
Ligthen a stream dataset by keeping only the event 
that are not ObjectType 3, ie contextual display.

TASK 
Pull the click column from testSearchStream and store
it in a separate dataset.
"""

search_stream = Dataset(INPUTS[0])
columns = search_stream.columns

unixpipe = search_stream.unixpipe.pipe("""grep "\t3\t" """)
search_stream_grepped = Dataset(search_stream.name, columns=columns, unixpipe=unixpipe)

output_columns = [col for col in search_stream.columns if col != "ObjectType"]
object_type_cid = search_stream_grepped.col("ObjectType")
predicate = lambda row:int(row[object_type_cid]) == 3
search_stream_grepped\
	.filter(predicate)\
	.retain(output_columns)\
	.write(TARGET)
