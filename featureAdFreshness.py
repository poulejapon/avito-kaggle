from dataset import Dataset

Dataset("testSearchStreamLight")\
	.retain(["ID", "AdID"])\
	.hash_join("AdID", Dataset("adFreshness"))\
        .drop("AdID")\
	.write("featureAdFreshness")
